# Docker httpd server for MCD projects

### What does:
Builds `vascorola/mcd:1.0` image and runs it as `mcd` container. 

# 1. Config

Just edit the `.env` file and write your ip address (or payara server).


# 2. Build and Up
Run on project directory:
`docker-compose build --force-rm --no-cache ; docker-compose up -d; docker ps; `
It should list a container named `mcd`.

# Files

`httpd.conf`: replaces default apache configuration;

`httpd-vhosts.conf`: virtual hosts configuration for the each project;

`.env`: environment variables. edit to change your ip address (or payara server);



---

# Cleanup
After building and running:
to stop all containers `docker stop $(docker ps -a -q); ` or `docker stop mcd`