#!/bin/sh
go get -u github.com/gorilla/mux
go get -u gopkg.in/mgo.v2/bson
go get -u github.com/mlabouardy/movies-restapi/config
go get -u github.com/mlabouardy/movies-restapi/dao
go get -u github.com/mlabouardy/movies-restapi/models
go get -u gopkg.in/mgo.v2
go get -u github.com/BurntSushi/toml
go get -u github.com/gin-gonic/contrib/static
go get -u github.com/gin-gonic/gin
